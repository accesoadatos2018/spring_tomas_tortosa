/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo03.dominio;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Tomas
 */
public class Dia {

    private String date; // ATRIBUTO DE DIA
    private String periodo_Probabilidad_Precipitación;
    private int prob_precipitacion;
    private String periodo_Estado_Cielo; // ATRIBUTO PERIODO DE ESTADO CIELO
    private String descripcion; // ATRIBUTO DE ESTADO CIELO
    private String estado_cielo;
    //private Temperatura temperatura;
    //ArrayList<Precipitacion> listaProbabilidad;

    public Dia() {
    }

    public Dia(String date, String periodo_prob_prec, String periodo_Estado_Cielo, String descripcion, int prob_precipitacion, String estado_cielo) {
        this.date = date;
        this.periodo_Probabilidad_Precipitación = periodo_prob_prec;
        this.periodo_Estado_Cielo = periodo_Estado_Cielo;
        this.descripcion = descripcion;
        this.prob_precipitacion = prob_precipitacion;
        this.estado_cielo = estado_cielo;

    }

    public int getProb_precipitacion() {
        return prob_precipitacion;
    }

    public void setProb_precipitacion(int prob_precipitacion) {
        this.prob_precipitacion = prob_precipitacion;
    }

    public String getEstado_cielo() {
        return estado_cielo;
    }

    public void setEstado_cielo(String estado_cielo) {
        this.estado_cielo = estado_cielo;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPeriodo_Estado_Cielo() {
        return periodo_Estado_Cielo;
    }

    public void setPeriodo_Estado_Cielo(String periodo_Estado_Cielo) {
        this.periodo_Estado_Cielo = periodo_Estado_Cielo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

}
