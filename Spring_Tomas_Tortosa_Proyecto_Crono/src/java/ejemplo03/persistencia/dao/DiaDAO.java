package ejemplo03.persistencia.dao;

import ejemplo03.dominio.Dia;
import com.fpmislata.persistencia.dao.GenericDAO;

public interface DiaDAO extends GenericDAO<Dia,String> {

}
