package ejemplo03.persistencia.dao.impl;

import ejemplo03.persistencia.dao.DiaDAO;
import ejemplo03.dominio.Dia;
import com.fpmislata.persistencia.dao.impl.GenericDAOImplHibernate;

public class DiaDAOImplHibernate extends GenericDAOImplHibernate<Dia,String> implements  DiaDAO {

}
