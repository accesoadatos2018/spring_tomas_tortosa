/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo03.presentacion;

import com.fpmislata.persistencia.dao.BussinessException;
import com.fpmislata.persistencia.dao.BussinessMessage;
import ejemplo03.dominio.Dia;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import ejemplo03.persistencia.dao.DiaDAO;

/**
 *
 * @author Tomas
 */
@Controller
public class DiaController {

    @Autowired
    private DiaDAO diaDAO;
    
    @RequestMapping({"/index.html"})
    public ModelAndView listarProfesores(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> model = new HashMap<String, Object>();
        String viewName;

        try {
            List<Dia> dias=diaDAO.findAll();
            model.put("dias",dias);
            viewName = "diaLista";
        } catch (BussinessException ex) {
            model.put("bussinessMessages", ex.getBussinessMessages());
            model.put("backURL", request.getContextPath() + "/index.html");
            viewName = "error";
        }

        return new ModelAndView(viewName, model);
    }
    @RequestMapping({"/dia/newForInsert"})
    public ModelAndView newForInsert(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> model = new HashMap<String, Object>();
        String viewName;

        try {
            Dia dia = diaDAO.create();
            model.put("formOperation", FormOperation.Insert);
            model.put("dia", dia);
            viewName = "dia";
        } catch (BussinessException ex) {
            model.put("bussinessMessages", ex.getBussinessMessages());
            model.put("backURL", request.getContextPath() + "/index.html");
            viewName = "error";
        }

        return new ModelAndView(viewName, model);
    }

    @RequestMapping({"/dia/readForUpdate"})
    public ModelAndView readForUpdate(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> model = new HashMap<String, Object>();
        String viewName;

        try {
            String id;
            try {
                id = request.getParameter("date");
            } catch (NumberFormatException nfe) {
                throw new BussinessException(new BussinessMessage(null,"Se debe escribir un Id válido"));
            }

            Dia dia = diaDAO.get(id);
            if (dia == null) {
                throw new BussinessException(new BussinessMessage(null, "No existe el dia con id=" + id));
            }
            model.put("formOperation", FormOperation.Update);
            model.put("dia", dia);
            viewName = "dia";
        } catch (BussinessException ex) {
            model.put("bussinessMessages", ex.getBussinessMessages());
            model.put("backURL", request.getContextPath() + "/index.html");
            viewName = "error";
        }

        return new ModelAndView(viewName, model);
    }

    @RequestMapping({"/dia/readForDelete"})
    public ModelAndView readForDelete(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> model = new HashMap<String, Object>();
        String viewName;
        try {
            String id;
            try {
                id = request.getParameter("date");
            } catch (NumberFormatException nfe) {
                throw new BussinessException(new BussinessMessage(null,"Se debe escribir un Id válido"));
            }

            Dia dia = diaDAO.get(id);
            if (dia == null) {
                throw new BussinessException(new BussinessMessage(null, "No existe el dia con id=" + id));
            }
            model.put("formOperation", FormOperation.Delete);
            model.put("dia", dia);
            viewName = "dia";
        } catch (BussinessException ex) {
            model.put("bussinessMessages", ex.getBussinessMessages());
            model.put("backURL", request.getContextPath() + "/index.html");
            viewName = "error";
        }

        return new ModelAndView(viewName, model);
    }

    @RequestMapping({"/dia/insert.html"})
    public ModelAndView insert(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> model = new HashMap<String, Object>();
        String viewName;

        try {
            request.setCharacterEncoding("UTF-8");
        } catch (UnsupportedEncodingException ex) {
            throw new RuntimeException(ex);
        }

        Dia dia = null;
        try {
            dia = diaDAO.create();

            dia.setDate(request.getParameter("date"));
            dia.setPeriodo_Estado_Cielo(request.getParameter("periodo_Estado_Cielo"));
            dia.setDescripcion(request.getParameter("descripcion"));
            dia.setEstado_cielo(request.getParameter("estado_cielo"));

            diaDAO.saveOrUpdate(dia);

            viewName = "redirect:/index.html";
        } catch (BussinessException ex) {
            model.put("bussinessMessages", ex.getBussinessMessages());
            if (dia!=null) {
                dia.setDate("");
            }
            model.put("dia", dia);
            model.put("formOperation", FormOperation.Insert);
            viewName = "dia";
        }



        return new ModelAndView(viewName, model);
    }

    @RequestMapping({"/dia/update.html"})
    public ModelAndView update(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> model = new HashMap<String, Object>();
        String viewName;
        try {
            request.setCharacterEncoding("UTF-8");
        } catch (UnsupportedEncodingException ex) {
            throw new RuntimeException(ex);
        }
        Dia dia = new Dia();
        try {
            String id;
            try {
                id = request.getParameter("date");
            } catch (NumberFormatException nfe) {
                throw new BussinessException(new BussinessMessage(null,"Se debe escribir un Id válido"));
            }
            dia = diaDAO.get(id);
            if (dia == null) {
                throw new BussinessException(new BussinessMessage(null, "Ya no existe el dia."));
            }
            
            dia.setDate(request.getParameter("date"));
            dia.setPeriodo_Estado_Cielo(request.getParameter("periodo_Estado_Cielo"));
            dia.setDescripcion(request.getParameter("descripcion"));
            dia.setEstado_cielo(request.getParameter("estado_cielo"));

            diaDAO.saveOrUpdate(dia);

            viewName = "redirect:/index.html";
        } catch (BussinessException ex) {
            model.put("bussinessMessages", ex.getBussinessMessages());
            model.put("dia", dia);
            model.put("formOperation", FormOperation.Update);
            viewName = "dia";
        }

        return new ModelAndView(viewName, model);
    }

    @RequestMapping({"/dia/delete.html"})
    public ModelAndView delete(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> model = new HashMap<String, Object>();
        String viewName;

        Dia dia=null;
        try {
            String id;
            try {
                id = request.getParameter("date");
            } catch (NumberFormatException nfe) {
                throw new BussinessException(new BussinessMessage(null,"Se debe escribir un Id válido"));
            }
            dia = diaDAO.get(id);
            if (dia == null) {
                throw new BussinessException(new BussinessMessage(null, "Ya no existe el dia a borrar"));
            }

            diaDAO.delete(id);

            viewName = "redirect:/index.html";
        } catch (BussinessException ex) {
            model.put("bussinessMessages", ex.getBussinessMessages());
            model.put("dia", dia);
            model.put("formOperation", FormOperation.Delete);
            viewName = "dia";
        }

        return new ModelAndView(viewName, model);
    }

}