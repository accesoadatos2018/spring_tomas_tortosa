<%@page import="ejemplo03.dominio.Dia"%>
<%@page import="org.springframework.web.util.HtmlUtils"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    List<Dia> dias = (List<Dia>) request.getAttribute("dias");
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Dia</title>
        <link href="<%=request.getContextPath()%>/css/bootstrap.css" rel="stylesheet">
        <link href="<%=request.getContextPath()%>/css/bootstrap-responsive.css" rel="stylesheet">
        <script type="text/javascript"  src="<%=request.getContextPath()%>/js/jquery-1.9.0.js"></script>
        <script type="text/javascript"  src="<%=request.getContextPath()%>/js/bootstrap.js" ></script>
    </head>
    <body style="background:#FDFDFD">
        <div class="row-fluid">
            <div class="span12">&nbsp;</div>
        </div>
        <div class="row-fluid">
            <div class="offset1  span10">
                <div class="row-fluid">
                    <div class="span12">
                        <a id="btnNuevo" class="btn btn-primary" href="<%=request.getContextPath()%>/dia/newForInsert.html">Nuevo Dia</a>
                    </div>
                </div>
                <div class="row-fluid">


                    <div class="span12">



                        <table class="table table-bordered table-hover table-condensed">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Periodo_Estado_Cielo</th>
                                    <th>Descripcion</th>
                                    <th>Estado_Cielo</th>
                                </tr>
                            </thead>
                            <tbody>
                                <%
                                    for (Dia d : dias) {
                                %>
                                <tr>
                                    <td><a href="<%=request.getContextPath()%>/dia/readForUpdate.html?date=<%=d.getDate()%>" title="Editar" ><%=d.getDate()%></a></td>
                                    <td><%=HtmlUtils.htmlEscape(d.getPeriodo_Estado_Cielo())%></td>
                                    <td><%=HtmlUtils.htmlEscape(d.getDescripcion())%></td>
                                    <td><%=HtmlUtils.htmlEscape(d.getEstado_cielo())%></td>
                                    <td>
                                        <a href="<%=request.getContextPath()%>/dia/readForDelete.html?date=<%=d.getDate()%>" title="Borrar" ><i class="icon-trash"></i></a>
                                    </td>
                                </tr>
                                <%
                                    }
                                %>
                            </tbody>
                        </table>


                    </div>
                </div>
            </div>
            <div class="span1"></div>
        </div>
    </body>
</html>