/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import modelo.Profesor;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

/**
 *
 * @author Sandra
 */
public class AppProfesor {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       SessionFactory sessionFactory;
       Configuration configuration = new Configuration();
       configuration.configure();
       
      // ServiceRegistry serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry();
       sessionFactory = configuration.buildSessionFactory();
       
       //creamos el objeto que queremos guardar
       Profesor profesor = new Profesor(1,"Pepe","García","Perez");
       
       //creamos una sesion
       Session session= sessionFactory.openSession();
       
       //creamos una transaccion (=o se hacen todas las operaciones o no se hacen)
       session.beginTransaction();
       
       //guardar objeto
       session.save(profesor);
       
       //cerrar la conexion
       session.getTransaction().commit();
       session.close();
     
    }
   
}
